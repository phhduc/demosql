const express = require('express');
const router = express.Router();
const controller = require('./controller');

// Định nghĩa tuyến API
router.get('/employees', controller.getAllEmployees);
router.get('/employees/:id', controller.getAllEmployeDetail);
router.get('/employees/number-departments', controller.getEmployeesAndNumberDepartments)
router.get('/departments/employees', controller.getEmployeesByDepartmentSlow )
router.get('/salary/sum', controller.calculateTotalSalarySlow)
module.exports = router;