const mysql = require('mysql');
require('dotenv').config()

const host = process.env.HOST;
const user = process.env.USER;
const password = process.env.PASSWORD;
const database = process.env.DB;
console.log('in', host, user, password, database)
// Cấu hình kết nối MySQL
const connection = mysql.createConnection({
  host,
  user,
  password,
  database,
});

// Kết nối đến cơ sở dữ liệu
connection.connect((err) => {
  if (err) {
    console.error('Lỗi kết nối đến cơ sở dữ liệu: ' + err.stack);
    return;
  }
  console.log('Đã kết nối thành công đến cơ sở dữ liệu Sakila!');
});

// Xuất kết nối cơ sở dữ liệu
module.exports = connection;