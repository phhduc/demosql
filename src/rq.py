import requests
import time
import argparse

# Parse command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('ver', help='Version number')
args = parser.parse_args()

# List of URLs
urls = [
    f"http://{args.ver}/api/employees",
    f"http://{args.ver}/api/employees/number-departments",
    f"http://{args.ver}/api/employees/1",
    f"http://{args.ver}/api/departments/employees"
]

# Number of times to send the request for each URL
num_requests = 1

total_response_time = 0

for url in urls:
    total_time = 0

    for _ in range(num_requests):
        start_time = time.time()

        # Send GET request
        response = requests.get(url)

        # Measure response time
        elapsed_time = time.time() - start_time
        total_time += elapsed_time

        # Display response time
        print(f"URL: {url} | Response Time: {elapsed_time:.2f} seconds")

    # Calculate average response time for the URL
    avg_response_time = total_time / num_requests
    print(f"Average Response Time for {url}: {avg_response_time:.2f} seconds\n")

    # Add response time to total response time
    total_response_time += total_time

print(f"Total Response Time for all requests: {total_response_time:.2f} seconds")
