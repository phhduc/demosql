const { query } = require('express');
const db = require('./db'); // Import kết nối cơ sở dữ liệu từ tệp db.js

exports.getAllEmployees = (req, res) => {
  const page = parseInt(req.query.page) || 1; // Trang hiện tại, mặc định là 1
  const limit = parseInt(req.query.limit) || 10; // Số lượng nhân viên trên mỗi trang, mặc định là 10
  const offset = (page - 1) * limit; // Số bản ghi cần bỏ qua
  
  const columns = req.query.columns || '*'; // Danh sách cột mặc định nếu không được cung cấp
  
  // Tạo mảng các cột
  const columnArray = columns.split(',');
  
  // Tạo chuỗi cột trong truy vấn SQL
  const columnString = columnArray.join(',');
  
  // Thực hiện truy vấn SQL để lấy thông tin nhân viên với phân trang và các cột chỉ định
  const query = `SELECT ${columnString} FROM employees LIMIT ${limit} OFFSET ${offset}`; 
  
  db.query(`SELECT * FROM employees ${''}${''}${''}`, (err, results) => {
    if (err) {
      console.error('Lỗi truy vấn cơ sở dữ liệu: ', err);
      res.status(500).json({ error: 'Lỗi truy vấn cơ sở dữ liệu' });
      return;
    }
    res.json(results);
  });
}

exports.getEmployeesAndNumberDepartments = (req, res) => {
  const page = parseInt(req.query.page) || 1; // Trang hiện tại, mặc định là 1
  const limit = parseInt(req.query.limit) || 10; // Số lượng nhân viên trên mỗi trang, mặc định là 10
  const offset = (page - 1) * limit;
  db.query(
    `SELECT e.emp_no, e.first_name, e.last_name, COUNT(DISTINCT d.dept_no) AS num_departments
    FROM employees e
    INNER JOIN dept_emp d ON e.emp_no = d.emp_no
    GROUP BY e.emp_no ${''}${''}`, (err, results) => {
    if (err) {
      console.error('Lỗi truy vấn cơ sở dữ liệu: ', err);
      res.status(500).json({ error: 'Lỗi truy vấn cơ sở dữ liệu' });
      return;
    }
    res.json(results);
  })
}

exports.getAllEmployeDetail = (req, res) => {
  const employeeId = req.params.id; // ID của nhân viên
  
  const query = `SELECT * FROM employees
                 JOIN titles ON employees.emp_no = titles.emp_no
                 JOIN salaries ON employees.emp_no = salaries.emp_no
                 WHERE employees.emp_no = ?`;

  db.query(query, [employeeId], (err, results) => {
    if (err) {
      console.error('Lỗi truy vấn cơ sở dữ liệu: ', err);
      res.status(500).json({ error: 'Lỗi truy vấn cơ sở dữ liệu' });
      return;
    }
    res.json(results);
  });
};


exports.getEmployeesByDepartmentSlow = (req, res) => {
  const departmentId = req.params.departmentId; // ID của phòng ban
  
  const query = `SELECT * FROM employees
                 JOIN dept_emp ON employees.emp_no = dept_emp.emp_no
                 JOIN departments ON dept_emp.dept_no = departments.dept_no
                 WHERE departments.dept_id = ?`;

  db.query(query, [departmentId], (err, results) => {
    if (err) {
      console.error('Lỗi truy vấn cơ sở dữ liệu: ', err);
      res.status(500).json({ error: 'Lỗi truy vấn cơ sở dữ liệu' });
      return;
    }
    res.json(results);
  });
};
exports.calculateTotalSalarySlow = (req, res) => {
  const query = `SELECT SUM(salary) AS total_salary FROM salaries`;

  db.query(query, (err, results) => {
    if (err) {
      console.error('Lỗi truy vấn cơ sở dữ liệu: ', err);
      res.status(500).json({ error: 'Lỗi truy vấn cơ sở dữ liệu' });
      return;
    }
    const totalSalary = results[0].total_salary;
    res.json({ total_salary: totalSalary });
  });
};
