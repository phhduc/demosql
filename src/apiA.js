const express = require('express');
const router = express.Router();
const controller = require('./ctrl');

// Định nghĩa tuyến API
router.get('/employees', controller.getAllEmployees);
router.get('/employees/number-departments', controller.getEmployeesAndNumberDepartments)
router.get('/employees/:id', controller.getAllEmployeDetail);
router.get('/departments/employees', controller.getEmployeesByDepartmentOptimized )
router.get('/salary/sum', controller.calculateTotalSalaryOptimized)
module.exports = router;