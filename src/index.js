const express = require('express');
const morgan = require('morgan')
const apiRoutes = require('./api');
const apiV2 = require('./apiA')
const fs = require('fs');
const path = require('path');
const moment = require('moment');
const readline = require('readline');
require('moment-timezone');

const app = express();
const logStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' });
// Custom log format function
morgan.token('date', (req, res, tz) => {
  return moment().tz(tz).format();
});
morgan.token('cpu', () => {
  const cpuUsage = process.cpuUsage();
  return `${cpuUsage.user + cpuUsage.system}µs`;
});
morgan.token('ram', () => {
  const usedMemory = process.memoryUsage().rss;
  return `${(usedMemory / 1024 / 1024).toFixed(2)}MB`;
});
// Custom log format string
const logFormat = ':method :url :status [:date[Asia/Bangkok]] - :cpu :ram :response-time ms' ;

// Custom middleware function to log and save the information
function logMiddleware(req, res, next) {
  morgan(logFormat, { stream: logStream })(req, res, next);
}

// Attach the custom middleware
app.use(logMiddleware);
app.get('/logs', (req, res) => {
  const logFilePath = path.join(__dirname, 'access.log');
  const lines = [];

  // Create a readline interface for reading the log file
  const rl = readline.createInterface({
    input: fs.createReadStream(logFilePath),
    crlfDelay: Infinity
  });

  // Read the file line by line
  rl.on('line', (line) => {
    lines.push(line);
  });

  // Once all lines are read, send the last 10 lines as the API response
  rl.on('close', () => {
    const lastTenLines = lines.slice(-10);
    res.json(lastTenLines);
  });
});

// Đăng ký tuyến API
app.use('/v1/api', apiRoutes);
app.use('/v2/api', apiV2);

// Lắng nghe các yêu cầu
app.listen(3000, () => {
  console.log('Máy chủ Express đang lắng nghe trên cổng 3000');
});