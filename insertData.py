import mysql.connector
from faker import Faker
import random
from datetime import datetime

# Cập nhật các giá trị kết nối cơ sở dữ liệu
host = '167.172.94.132'
database = 'sakila'
user = 'doan'
password = 'CTK43doantotnghiep!'

def connect_to_database():
    cnx = mysql.connector.connect(host=host, database=database, user=user, password=password)
    return cnx

def insert_random_rows(table_name, column_names, num_rows, cnx):
    cursor = cnx.cursor()
    insert_query = f"INSERT INTO {table_name} ({', '.join(column_names)}) VALUES ({', '.join(['%s']*len(column_names))})"

    fake = Faker()

    for _ in range(num_rows):
        row = []

        for column in column_names:
            if column == 'last_update':
                row.append(datetime.now())
            elif column == 'address':
                row.append(fake.address())
            elif column == 'email':
                row.append(fake.email())
            elif column == 'first_name':
                row.append(fake.first_name())
            elif column == 'last_name':
                row.append(fake.last_name())
            elif column == 'picture':
                row.append(None)  # Sử dụng giá trị NULL cho trường GEOMETRY
            else:
                row.append(random.choice(range(1, 1000)))

        cursor.execute(insert_query, tuple(row))
    cnx.commit()

def insert_rows_to_all_tables(cnx):
    cursor = cnx.cursor()
    cursor.execute("SHOW TABLES")
    tables = cursor.fetchall()

    table_columns = {}
    for table in tables:
        table_name = table[0]
        if table_name != 'actor_info':  # Bỏ qua bảng actor_info
            cursor.execute(f"DESCRIBE {table_name}")
            columns = cursor.fetchall()
            column_names = [column[0] for column in columns]
            table_columns[table_name] = column_names

    for table_name, column_names in table_columns.items():
        insert_random_rows(table_name, column_names, 1000, cnx)

    cursor.close()

def main():
    cnx = connect_to_database()
    insert_rows_to_all_tables(cnx)
    cnx.close()

if __name__ == "__main__":
    main()
